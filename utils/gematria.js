export function convertToGematria(number) {
  const inputString = number.toString();
  const gematriaInts = ["א", "ב", "ג", "ד", "ה", "ו", "ז", "ח", "ט"];
  const gematriaTens = ["י", "כ", "ל", "מ", "נ", "ס", "ע", "פ", "צ"];
  const gematriaHundreds = [
    "ק",
    "ר",
    "ש",
    "ת",
    "תק",
    "תר",
    "תש",
    "תת",
    "תתק",
    "תתר",
  ];

  const gemOnes = parseInt(inputString[inputString.length - 1]);
  const gemTens = parseInt(inputString[inputString.length - 2]);
  const gemHuns = parseInt(inputString[inputString.length - 3]);

  let gemMeah = gematriaHundreds[gemHuns - 1];
  let gemAsra = gematriaTens[gemTens - 1];
  let gemEchad = gematriaInts[gemOnes - 1];

  gemEchad = gemEchad === undefined ? "" : gemEchad;
  gemAsra = gemAsra === undefined ? "" : gemAsra;
  gemMeah = gemMeah === undefined ? "" : gemMeah;

  if (gemAsra === "י" && gemEchad === "ה") {
    gemEchad = "ו";
    gemAsra = "ט";
  } else if (gemAsra === "י" && gemEchad === "ו") {
    gemEchad = "ז";
    gemAsra = "ט";
  }

  return gemMeah + gemAsra + gemEchad;
}

export function amud(page) {
  return page % 2 !== 0 ? "עמוד א" : "עמוד ב";
}

export function formatAmud(page) {
  return `${convertToGematria(parseInt(page / 2) + 2)} עמוד ${
    page % 2 !== 0 ? "א" : "ב"
  }`;
}
