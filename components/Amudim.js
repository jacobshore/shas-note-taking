import AmudOptions from "./AmudOptions";
import Grid from "@mui/material/Grid";
import { convertToGematria } from "../utils/gematria";
import useNotes from "../hooks/useNotes";

function listDapimAndAmudim(meseches, pages) {
  const dapim = [];
  for (let i = 0; i < pages; i++) {
    dapim.push({
      meseches,
      daf: convertToGematria(parseInt(i / 2) + 2),
      amud: i % 2 === 0 ? "א" : "ב",
      page: i + 1,
    });
  }
  return dapim;
}

export default function Amudim({ meseches, pages }) {
  const { data: notes } = useNotes(meseches);
  const dapim = listDapimAndAmudim(meseches, pages);
  return (
    <Grid container spacing={2} sx={{ flexGrow: 1, padding: "10px" }}>
      {dapim.map((x, i) => (
        <Grid item xs={12} md={4} lg={3} key={i}>
          <AmudOptions
            amudInfo={x}
            meseches={meseches}
            amud={i + 1}
            notes={notes ? notes.filter((note) => note?.amud === i + 1) : []}
          />
        </Grid>
      ))}
    </Grid>
  );
}
