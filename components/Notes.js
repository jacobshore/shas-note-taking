import {
  AppBar,
  Button,
  Dialog,
  DialogContent,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";

import CloseIcon from "@mui/icons-material/Close";
import { Note } from "./Note";
import { useState } from "react";

export function Notes({ meseches, daf, amud, notes }) {
  const [visible, setVisible] = useState(false);
  function toggle() {
    setVisible(!visible);
  }
  return (
    <>
      <Button onClick={toggle} variant="outlined" size="large">
        ראה הערות
      </Button>
      <Dialog
        fullScreen
        title="הערות"
        layout="vertical"
        style={{ direction: "rtl" }}
        open={visible}
        onCancel={toggle}
        onConfirm={toggle}
      >
        <AppBar sx={{ position: "relative", direction: "rtl" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={toggle}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography
              sx={{ mr: 2, flex: 1, flexDirection: "reverse" }}
              variant="h6"
              component="div"
            >
              {meseches} {daf} {amud % 2 !== 0 ? "." : ":"}
            </Typography>
          </Toolbar>
        </AppBar>
        <DialogContent>
          {notes.map((note, i) => (
            <Note key={i} note={note} />
          ))}
        </DialogContent>
      </Dialog>
    </>
  );
}
