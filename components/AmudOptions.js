import {
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Typography,
} from "@mui/material";

import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import NoteForm from "./NoteForm";
import { Notes } from "./Notes";
import RemoveCircleOutlineOutlinedIcon from "@mui/icons-material/RemoveCircleOutlineOutlined";
import { useState } from "react";

export default function AmudOptions({ amudInfo, notes }) {
  const [timesLearnt, setTimesLearnt] = useState(0);
  const { daf, amud, page, meseches } = amudInfo;

  return (
    <Card>
      <CardHeader title={`דף ${daf} עמוד ${amud}`} />
      <CardContent>
        <Notes meseches={meseches} daf={daf} amud={page} notes={notes} />
        <NoteForm meseches={meseches} daf={daf} amud={page} />
        <Typography variant="h3" style={{ margin: "10px" }}>
          {timesLearnt}
        </Typography>
        <Typography style={{ fontWeight: "bold" }}>פעמים</Typography>
        <div>
          <IconButton
            size="large"
            onClick={() => setTimesLearnt(timesLearnt + 1)}
          >
            <AddCircleOutlineOutlinedIcon />
          </IconButton>
          <IconButton
            size="large"
            onClick={() =>
              timesLearnt > 0
                ? setTimesLearnt(timesLearnt - 1)
                : setTimesLearnt(0)
            }
          >
            <RemoveCircleOutlineOutlinedIcon />
          </IconButton>
        </div>
      </CardContent>
    </Card>
  );
}
