import {
  AppBar,
  Dialog,
  IconButton,
  Paper,
  TextField,
  Toolbar,
  Typography,
} from "@mui/material";

import { Box } from "@mui/system";
import { Button } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { supabase } from "../utils/initSupabase";
import { useQueryClient } from "react-query";
import { useState } from "react";

export default function NoteForm({ meseches, daf, amud }) {
  const [note, setNote] = useState("");
  const [title, setTitle] = useState("");
  const { id: user_id } = supabase.auth.user();
  const [visible, setVisible] = useState(false);
  function toggle() {
    setVisible(!visible);
  }

  const queryClient = useQueryClient();

  const handleClose = () => setVisible(false);
  const createNote = async (note) => {
    try {
      const { data, error } = await supabase
        .from("shas_note")
        .insert([{ meseches, amud, title, body: note, user_id }], {
          returning: "minimal",
        });
      setTitle("");
      setNote("");
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Button onClick={toggle} variant="outlined" size="large">
        הוסף הערה
      </Button>
      <Dialog
        fullScreen
        open={visible}
        onClose={handleClose}
        style={{ direction: "rtl" }}
      >
        <AppBar sx={{ position: "relative", direction: "rtl" }}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography
              sx={{ mr: 2, flex: 1, flexDirection: "reverse" }}
              variant="h6"
              component="div"
            >
              {meseches} {daf} {amud % 2 !== 0 ? "." : ":"}
            </Typography>
            <Button
              autoFocus
              color="inherit"
              onClick={(e) => {
                e.preventDefault();
                toggle();
                queryClient.invalidateQueries(["notes", meseches]);
                createNote(note);
              }}
            >
              שמור
            </Button>
          </Toolbar>
        </AppBar>
        <Paper style={{ height: "100%" }}>
          <Box
            component="form"
            sx={{ display: "flex", flexDirection: "column", p: 5 }}
            noValidate
            autoComplete="off"
          >
            <TextField
              label="כותרת"
              value={title}
              variant="outlined"
              onChange={(e) => {
                setTitle(e.target.value);
              }}
            />
            <TextField
              multiline
              label="הערה"
              variant="outlined"
              rows={20}
              value={note}
              placeHolder="הקלד כאן"
              onChange={(e) => setNote(e.target.value)}
            />
          </Box>
          <Box sx={{ textAlign: "center" }}>
            <Button
              variant="contained"
              size="large"
              onClick={(e) => {
                e.preventDefault();
                toggle();
                createNote(note);
              }}
            >
              שמור
            </Button>
          </Box>
        </Paper>
      </Dialog>
    </>
  );
}
