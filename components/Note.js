import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import Typography from "@mui/material/Typography";
import dayjs from "dayjs";

export function Note({ note }) {
  const date_created = dayjs(date_created).format("MMMM DD, YYYY");
  return (
    <Card sx={{ direction: "rtl" }}>
      <CardHeader title={note.title} subheader={date_created} />
      <CardContent>
        <Typography sx={{ whiteSpace: "pre-wrap" }}>{note.body}</Typography>
      </CardContent>
    </Card>
  );
}
