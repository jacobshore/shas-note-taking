# Shas Notes

An app to aid in taking notes and keeping track of progress while studying Talmud

Currently in very early development (project started September 2021). Documentation will soon be available.

For now, the app in dev form [is being hosted here](https://shas-notes.netlify.app/)