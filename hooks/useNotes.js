import { supabase } from "../utils/initSupabase";
import { useQuery } from "react-query";

const getNotes = async () => {
  const { data, error } = await supabase.from("shas_note").select();

  if (error) throw new Error(error.message);
  if (!data) throw new Error("Notes not found!");
  return data;
};

const getMesechesNotes = async (meseches) => {
  const { data, error } = await supabase
    .from("shas_note")
    .select()
    .eq("meseches", meseches);
  if (error) throw new Error(error.message);
  if (!data) throw new Error("Notes not found!");
  return data;
};

export default function useNotes(meseches = null) {
  if (meseches === null) return useQuery("notes", getNotes);
  return useQuery(["notes", meseches], () => getMesechesNotes(meseches));
}
