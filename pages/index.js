import { Auth, Button, Card, Icon, Space, Typography } from "@supabase/ui";
import { useEffect, useState } from "react";

import Head from "next/head";
import Link from "next/link";
import { supabase } from "../utils/initSupabase";
import useSWR from "swr";

const fetcher = (url, token) =>
  fetch(url, {
    method: "GET",
    headers: new Headers({ "Content-Type": "application/json", token }),
    credentials: "same-origin",
  }).then((res) => res.json());

const Index = () => {
  const { user, session } = Auth.useUser();
  const { data, error } = useSWR(
    session ? ["/api/getUser", session.access_token] : null,
    fetcher
  );
  const [authView, setAuthView] = useState("sign_in");

  useEffect(() => {
    const { data: authListener } = supabase.auth.onAuthStateChange(
      (event, session) => {
        if (event === "PASSWORD_RECOVERY") setAuthView("update_password");
        if (event === "USER_UPDATED")
          setTimeout(() => setAuthView("sign_in"), 1000);
        // Send session to /api/auth route to set the auth cookie.
        // NOTE: this is only needed if you're doing SSR (getServerSideProps)!
        fetch("/api/auth", {
          method: "POST",
          headers: new Headers({ "Content-Type": "application/json" }),
          credentials: "same-origin",
          body: JSON.stringify({ event, session }),
        }).then((res) => res.json());
      }
    );

    return () => {
      authListener.unsubscribe();
    };
  }, []);

  const View = () => {
    if (!user)
      return (
        <>
          <Head>
            <title>Shas Notes: A note taking app for the Talmudist</title>
            <meta
              name="description"
              content="Shas Notes is a place where aims to give you tools and encouragement
          as you learn through Shas."
            />
            <meta name="author" content="Jacob Shore" />
          </Head>
          <Space direction="vertical" size={8}>
            <div>
              <Typography.Title level={3}>
                Welcome to Shas Notes
              </Typography.Title>
            </div>
            <Auth
              supabaseClient={supabase}
              providers={["google"]}
              view={authView}
              socialLayout="horizontal"
              socialButtonSize="xlarge"
            />
          </Space>
        </>
      );

    return (
      <>
        <Head>
          <title>Shas Notes: A note taking app for the Talmudist</title>
          <meta
            name="description"
            content="Shas Notes is a place where aims to give you tools and encouragement
            as you learn through Shas."
          />
          <meta name="author" content="Jacob Shore" />
        </Head>
        <Space direction="vertical" size={6}>
          {authView === "update_password" && (
            <Auth.UpdatePassword supabaseClient={supabase} />
          )}
          {user && (
            <>
              <Typography.Text>You're signed in</Typography.Text>
              <Typography.Title level={2} strong>
                Welcome {data?.user_metadata?.full_name ?? user.email}!
              </Typography.Title>
              <Typography.Text>
                <Link href="/shas">
                  <Button size="xlarge" type="outline">
                    Learn Shas
                  </Button>
                </Link>
              </Typography.Text>
              <Button
                icon={<Icon type="LogOut" />}
                type="outline"
                onClick={() => supabase.auth.signOut()}
              >
                Log out
              </Button>
              {error && (
                <Typography.Text danger>Failed to fetch user!</Typography.Text>
              )}
              {data && !error ? (
                <>
                  <Typography.Text type="success">
                    User data retrieved server-side (in API route):
                  </Typography.Text>
                </>
              ) : (
                <div>Loading...</div>
              )}

              <Typography.Text>
                <Link href="/profile">
                  <a>SSR example with getServerSideProps</a>
                </Link>
              </Typography.Text>
            </>
          )}
        </Space>
      </>
    );
  };

  return (
    <div style={{ maxWidth: "420px", margin: "96px auto" }}>
      <Card>
        <View />
      </Card>
    </div>
  );
};

export default Index;
