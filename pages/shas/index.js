import {
  Card,
  CardContent,
  CardHeader,
  Grid,
  Link as MuiLink,
  Typography,
} from "@mui/material";

import Head from "next/head";
import Link from "next/link";
import styles from "./shas.module.css";
import { supabase } from "../../utils/initSupabase";
import tractates from "../../store/tractates.json";

// import useNotes from "../../hooks/useNotes";

export default function Shas() {
  const user = supabase.auth.user();
  // const { data } = useNotes();
  return (
    <>
      <Head>
        <title>Shas Notes</title>
      </Head>
      <div className={styles.shas}>
        <Typography>
          משתמש: {user?.user_metadata?.full_name ?? user?.email}
        </Typography>
        <Typography variant="h2" sx={{ textAlign: "center", p: 5 }}>
          Shas Notes
        </Typography>
        <Grid container spacing={2} sx={{ flexGrow: 1 }}>
          {tractates.map((meseches, i) => (
            <Grid item xs={6} md={4} lg={3} key={i}>
              <Card>
                <CardHeader
                  title={
                    <Typography variant="h4">
                      <Link
                        stlyes={{ color: "white" }}
                        href={`/shas/${meseches.name}`}
                        passHref
                      >
                        <MuiLink> {meseches.name}</MuiLink>
                      </Link>
                    </Typography>
                  }
                >
                  {" "}
                </CardHeader>
                <CardContent>
                  <Typography variant="h5">סדר: {meseches.seder}</Typography>
                  <Typography>{meseches.pages} עמודים</Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    </>
  );
}
