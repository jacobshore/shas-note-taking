import Amudim from "../../components/Amudim";
import Head from "next/head";
import { Typography } from "@mui/material";
import styles from "./shas.module.css";
import { supabase } from "../../utils/initSupabase";
import tractates from "../../store/tractates.json";
import { useRouter } from "next/router";

export default function Meseches() {
  const router = useRouter();
  const { meseches } = router.query;
  const pages = tractates.find((tractate) => tractate.name === meseches)?.pages;
  const user = supabase.auth.user();

  return (
    <>
      <Head>
        <title>Shas Notes: מסכת {meseches}</title>
      </Head>
      <div className={styles.shas} style={{ textAlign: "center" }}>
        <Typography>
          משתמש: {user?.user_metadata?.full_name ?? user?.email}
        </Typography>
        <Typography variant="h1">מסכת {meseches}</Typography>

        <Typography>0% נלמד</Typography>
        <Amudim meseches={meseches} pages={pages} />
      </div>
    </>
  );
}
