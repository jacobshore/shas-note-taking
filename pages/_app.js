import "./../style.css";

import { QueryClient, QueryClientProvider } from "react-query";

import { Auth } from "@supabase/ui";
import { supabase } from "../utils/initSupabase";

const queryClient = new QueryClient();

export default function MyApp({ Component, pageProps }) {
  return (
    <main className={"light"}>
      <Auth.UserContextProvider supabaseClient={supabase}>
        <QueryClientProvider client={queryClient}>
          <Component {...pageProps} />
        </QueryClientProvider>
      </Auth.UserContextProvider>
    </main>
  );
}
