import { Card, Space, Typography } from "@supabase/ui";

import { supabase } from "../utils/initSupabase";

export default function Profile({ user }) {
  return (
    <div style={{ maxWidth: "420px", margin: "96px auto" }}>
      <Card>
        <Space direction="vertical" size={6}>
          <Typography.Text>You're signed in!</Typography.Text>
          <Typography.Title level={2} strong>
            Welcome {user?.user_metadata?.full_name ?? user.email}!
          </Typography.Title>
          <Typography.Text type="success">
            User data retrieved server-side (from Cookie in getServerSideProps):
          </Typography.Text>
        </Space>
      </Card>
    </div>
  );
}

export async function getServerSideProps({ req }) {
  const { user } = await supabase.auth.api.getUserByCookie(req);

  if (!user) {
    // If no user, redirect to index.
    return { props: {}, redirect: { destination: "/", permanent: false } };
  }

  // If there is a user, return it.
  return { props: { user } };
}
