import { Card } from "@mui/material";
import useNotes from "../../hooks/useNotes";

export default function Notes() {
  const { data: notes, isLoading } = useNotes();
  if (isLoading) return <div>Loading...</div>;
  return (
    <div style={{ direction: "rtl" }}>
      {notes.map((note) => (
        <Card key={note.id}>
          <div>
            <h1>{note.title}</h1>
            <p>{note.body}</p>
          </div>
        </Card>
      ))}
    </div>
  );
}
